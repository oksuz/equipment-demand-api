/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE DATABASE IF NOT EXISTS booking_equipment;

# Dump of table booking
# ------------------------------------------------------------

USE booking_equipment;

DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_from` int(11) DEFAULT NULL,
  `return_to` int(11) DEFAULT NULL,
  `campervan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `book_date` datetime NOT NULL,
  `return_date` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E00CEDDEA087EA78` (`book_from`),
  KEY `IDX_E00CEDDE4ACAA341` (`return_to`),
  CONSTRAINT `FK_E00CEDDE4ACAA341` FOREIGN KEY (`return_to`) REFERENCES `location` (`id`),
  CONSTRAINT `FK_E00CEDDEA087EA78` FOREIGN KEY (`book_from`) REFERENCES `location` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;

INSERT INTO `booking` (`id`, `book_from`, `return_to`, `campervan`, `book_date`, `return_date`, `created_at`, `deleted_at`)
VALUES
	(2,1,1,'Surfer Suite','2021-03-08 00:00:00','2021-03-11 00:00:00','2021-03-06 14:23:04',NULL),
	(3,1,1,'Surfer Suite Deluxe','2021-03-09 00:00:00','2021-03-17 00:00:00','2021-03-07 10:10:29',NULL),
	(4,1,1,'Travel Home','2021-03-10 00:00:00','2021-03-20 00:00:00','2021-03-07 10:10:29',NULL),
	(5,1,1,'Travel Home Deluxe','2021-03-12 00:00:00','2021-03-17 00:00:00','2021-03-07 10:10:29',NULL),
	(6,3,2,'Road House Eliseo','2021-03-12 00:00:00','2021-03-15 00:00:00','2021-03-07 10:10:29',NULL),
	(7,2,1,'Camper Cabin','2021-03-15 00:00:00','2021-03-21 00:00:00','2021-03-07 10:10:29',NULL);

/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table booking_equipment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `booking_equipment`;

CREATE TABLE `booking_equipment` (
  `booking_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`booking_id`,`equipment_id`),
  KEY `IDX_400A1E803301C60` (`booking_id`),
  KEY `IDX_400A1E80517FE9FE` (`equipment_id`),
  CONSTRAINT `FK_400A1E803301C60` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_400A1E80517FE9FE` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `booking_equipment` WRITE;
/*!40000 ALTER TABLE `booking_equipment` DISABLE KEYS */;

INSERT INTO `booking_equipment` (`booking_id`, `equipment_id`, `quantity`, `created_at`, `deleted_at`)
VALUES
	(2,1,2,'2021-03-08 00:24:37',NULL),
	(5,6,5,'2021-03-08 00:25:34',NULL),
	(6,3,5,'2021-03-08 00:07:34',NULL),
	(7,3,5,'2021-03-08 00:07:36',NULL);

/*!40000 ALTER TABLE `booking_equipment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table doctrine_migration_versions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `doctrine_migration_versions`;

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`)
VALUES
	('DoctrineMigrations\\Version20210306135543','2021-03-06 13:55:46',241),
	('DoctrineMigrations\\Version20210306142035','2021-03-06 14:21:00',211);

/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table equipment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `equipment`;

CREATE TABLE `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D338D5835E9E89CB` (`location`),
  CONSTRAINT `FK_D338D5835E9E89CB` FOREIGN KEY (`location`) REFERENCES `location` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `equipment` WRITE;
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;

INSERT INTO `equipment` (`id`, `location`, `name`, `quantity`, `created_at`, `deleted_at`)
VALUES
	(1,1,'Portable Toilets',5,'2021-03-06 17:02:23',NULL),
	(2,1,'Bed Sheets',10,'2021-03-06 17:02:34',NULL),
	(3,3,'Sleeping Bags',10,'2021-03-07 23:53:37',NULL),
	(4,2,'Camping Tables',7,'2021-03-07 23:53:34',NULL),
	(6,5,'Chairs',50,'2021-03-07 23:54:02',NULL);

/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table location
# ------------------------------------------------------------

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;

INSERT INTO `location` (`id`, `name`, `created_at`, `deleted_at`)
VALUES
	(1,'Berlin','2021-03-06 17:01:41',NULL),
	(2,'Amsterdam','2021-03-06 17:01:46',NULL),
	(3,'Milano','2021-03-06 17:01:53',NULL),
	(4,'Munich','2021-03-06 17:01:59',NULL),
	(5,'Cologne ','2021-03-07 23:52:24',NULL);

/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

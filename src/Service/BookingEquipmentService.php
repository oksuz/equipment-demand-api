<?php


namespace App\Service;

use App\Repository\BookingEquipmentRepository;

class BookingEquipmentService
{
    /**
     * @var BookingEquipmentRepository
     */
    private BookingEquipmentRepository $bookingEquipmentRepository;

    /**
     * BookingEquipmentService constructor.
     * @param BookingEquipmentRepository $bookingEquipmentRepository
     */
    public function __construct(BookingEquipmentRepository $bookingEquipmentRepository)
    {
        $this->bookingEquipmentRepository = $bookingEquipmentRepository;
    }

    public function getEquipmentDemands(): array
    {
        $equipmentDemands = $this->bookingEquipmentRepository->findEquipmentUsages() ?? [];
        foreach ($equipmentDemands as &$equipmentDemand) {
            $equipmentDemand['equipmentInUse'] = (int)($equipmentDemand['equipmentInUse']);
            $inventory = $equipmentDemand['inventorySize'] - $equipmentDemand['equipmentInUse'];
            $inventory = $inventory <= 0 ? 0 : $inventory;

            $requiredQuantity = $equipmentDemand['desiredQuantity'] - $inventory;
            $equipmentDemand['inventorySize'] = $inventory;
            $equipmentDemand['requiredQuantity'] = $requiredQuantity <= 0 ? 0 : $requiredQuantity;

            $eqFromRentalIds = null == $equipmentDemand['equipmentWillReturnFromRental']
                ? []
                : explode(',', $equipmentDemand['equipmentWillReturnFromRental']);

            $equipmentDemand['equipmentWillReturnFromRental'] = array_map('intval', $eqFromRentalIds);
        }

        return $equipmentDemands;
    }
}

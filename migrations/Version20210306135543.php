<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210306135543 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE booking (id INT AUTO_INCREMENT NOT NULL, book_from INT DEFAULT NULL, return_to INT DEFAULT NULL, campervan VARCHAR(255) NOT NULL, book_date DATETIME NOT NULL, return_date DATETIME NOT NULL, created_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_E00CEDDEA087EA78 (book_from), INDEX IDX_E00CEDDE4ACAA341 (return_to), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE booking_equipment (booking_id INT NOT NULL, equipment_id INT NOT NULL, quantity INT NOT NULL, INDEX IDX_400A1E803301C60 (booking_id), INDEX IDX_400A1E80517FE9FE (equipment_id), PRIMARY KEY(booking_id, equipment_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipment (id INT AUTO_INCREMENT NOT NULL, location INT DEFAULT NULL, name VARCHAR(255) NOT NULL, quantity INT NOT NULL, created_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_D338D5835E9E89CB (location), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEA087EA78 FOREIGN KEY (book_from) REFERENCES location (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE4ACAA341 FOREIGN KEY (return_to) REFERENCES location (id)');
        $this->addSql('ALTER TABLE booking_equipment ADD CONSTRAINT FK_400A1E803301C60 FOREIGN KEY (booking_id) REFERENCES booking (id)');
        $this->addSql('ALTER TABLE booking_equipment ADD CONSTRAINT FK_400A1E80517FE9FE FOREIGN KEY (equipment_id) REFERENCES equipment (id)');
        $this->addSql('ALTER TABLE equipment ADD CONSTRAINT FK_D338D5835E9E89CB FOREIGN KEY (location) REFERENCES location (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE booking_equipment DROP FOREIGN KEY FK_400A1E803301C60');
        $this->addSql('ALTER TABLE booking_equipment DROP FOREIGN KEY FK_400A1E80517FE9FE');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDEA087EA78');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE4ACAA341');
        $this->addSql('ALTER TABLE equipment DROP FOREIGN KEY FK_D338D5835E9E89CB');
        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE booking_equipment');
        $this->addSql('DROP TABLE equipment');
        $this->addSql('DROP TABLE location');
    }
}

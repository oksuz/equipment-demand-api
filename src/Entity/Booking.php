<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use DateTimeInterface;

/**
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=true, hardDelete=false)
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=5, max=255)
     */
    private string $campervan;

    /**
     * @ORM\Column(type="datetime", name="book_date")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private DateTimeInterface $bookDate;

    /**
     * @ORM\Column(type="datetime", name="return_date")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private DateTimeInterface $returnDate;

    /**
     * @Assert\NotBlank(message="Invalid location id. It doesn't belongs to any location")
     * @ORM\ManyToOne(targetEntity="App\Entity\Location")
     * @ORM\JoinColumn(name="book_from", referencedColumnName="id")
     */
    private ?Location $bookFrom;

    /**
     * @Assert\NotBlank(message="Invalid location id. It doesn't belongs to any location")
     * @ORM\ManyToOne(targetEntity="App\Entity\Location")
     * @ORM\JoinColumn(name="return_to", referencedColumnName="id")
     */
    private ?Location $returnTo;

    /**
     * @ORM\Column(type="datetime", nullable=true, name="created_at")
     * @Gedmo\Timestampable(on="create")
     */
    private ?DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true, name="deleted_at")
     */
    private ?DateTimeInterface $deletedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BookingEquipment", mappedBy="booking")
     */
    private Collection $bookingEquipment;

    public function __construct()
    {
        $this->bookingEquipment = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCampervan(): ?string
    {
        return $this->campervan;
    }

    public function setCampervan(string $campervan): self
    {
        $this->campervan = $campervan;

        return $this;
    }

    public function getBookDate(): DateTimeInterface
    {
        return $this->bookDate;
    }

    public function setBookDate(DateTimeInterface $bookDate): self
    {
        $this->bookDate = $bookDate;

        return $this;
    }

    public function getReturnDate(): DateTimeInterface
    {
        return $this->returnDate;
    }

    public function setReturnDate(DateTimeInterface $returnDate): self
    {
        $this->returnDate = $returnDate;

        return $this;
    }

    public function getBookFrom(): ?Location
    {
        return $this->bookFrom;
    }

    public function setBookFrom(?Location $bookFrom): self
    {
        $this->bookFrom = $bookFrom;

        return $this;
    }

    public function getReturnTo(): ?Location
    {
        return $this->returnTo;
    }

    public function setReturnTo(?Location $returnTo): self
    {
        $this->returnTo = $returnTo;

        return $this;
    }

    public function setDeletedAt(?DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getBookingEquipment(): Collection
    {
        return $this->bookingEquipment;
    }

    public function addBookingEquipment(BookingEquipment $bookingEquipment): self
    {
        if (!$this->bookingEquipment->contains($bookingEquipment)) {
            $this->bookingEquipment[] = $bookingEquipment;
            $bookingEquipment->setBooking($this);
        }

        return $this;
    }

    public function removeBookingEquipment(BookingEquipment $bookingEquipment): self
    {
        $this->bookingEquipment->removeElement($bookingEquipment);
        return $this;
    }
}

# Equipment Demand Dashboard Backend

## How to run

First thing first, this app needs a database in order to store the data. The `docker-compose up` command prepares all requirements for run the app.

- ### Docker Compose
App is ready to ship anywhere through `docker`, also has pre-configured docker-compose file.
You can just start with `docker-compose up` command, in few minute app will be accepting request on port `8080`

- ### Docker

Build image with following command in order to push image to any docker registry:
Let's assume the image name is `backend-api`

`docker build -t backend-api .`

In the above command going to install dependencies, prepare `APP_ENV`, and run tests automatically. Also, you can run test that modified/newly added with following command:

`docker run --rm -v${pwd}:/opt/app -eAPP_ENV=test backend-api php bin/phpunit`

### Running the docker image

`.env.prod` file expects following parameters as environment variable:

- APP_SECRET
- DB_USER
- DB_PASSWORD
- DB_HOST
- DB_PORT
- DB_NAME

considering to that docker run command should be:

```bash
docker run --name backend-api-app \
-e APP_SECRET=test123456 \
-e DB_USER=db-user \
-e DB_PASSWORD=password \
-e DB_HOST=ip|hostname \
-e DB_PORT=3306 \
-e DB_NAME=booking_equipment \
-d backend-api 
```

The above configuration need a separate nginx http server for handle http requests

- ### Run with PHP Dev Server For Development Purposes

#### Setting up database

set up your database connection, and required parameters  in `.env` (environment specific) file than run following two commands for create the tables and the schemas:

```bash
php bin/console doctrine:database:create --if-not-exists
php bin/console doctrine:migrations:migrate --no-interaction
```

```bash
php -S 0.0.0.0:8889 -t ./public
```
#### Running Test

```bash
php bin/phpunit
```

#### PSR Check
It has also psr checker script, see the psr output to run following command:

```bash
composer phpcs
```
<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTimeInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingEquipmentRepository")
 * @Gedmo\SoftDeleteable(timeAware=true, fieldName="deletedAt", hardDelete=false)
 */
class BookingEquipment
{

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\Booking", inversedBy="bookingEquipment")
     * @ORM\JoinColumn(referencedColumnName="id", name="booking_id", nullable=false, onDelete="CASCADE")
     */
    private ?Booking $booking;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\Equipment")
     * @ORM\JoinColumn(referencedColumnName="id", name="equipment_id", nullable=false)
     */
    private ?Equipment $equipment;

    /**
     * @ORM\Column(type="integer", name="quantity", nullable=false)
     */
    private int $quantity;

    /**
     * @ORM\Column(name="created_at", nullable=true, type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private ?DateTimeInterface $createdAt;

    /**
     * @ORM\Column(name="deleted_at", nullable=true, type="datetime")
     */
    private ?DateTimeInterface $deletedAt;

    public function getBooking(): ?Booking
    {
        return $this->booking;
    }

    public function getEquipment(): ?Equipment
    {
        return $this->equipment;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function setBooking(?Booking $booking): void
    {
        $this->booking = $booking;
    }

    public function setEquipment(?Equipment $equipment): self
    {
        $this->equipment = $equipment;
        return $this;
    }

    public function setCreatedAt(?DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function setDeletedAt(?DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }


}

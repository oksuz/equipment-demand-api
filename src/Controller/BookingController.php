<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Service\BookingEquipmentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class BookingController
 * @package App\Controller
 * @Route("/booking", condition="(request.headers.get('x-api-version') == '1')")
 */
class BookingController extends AbstractController
{

    /**
     * @Route(methods={"POST"}, name="newBooking")
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @return Response
     * @throws \Exception|ExceptionInterface
     */
    public function newBooking(
        Request $request,
        ValidatorInterface $validator,
        SerializerInterface $serializer
    ): Response {
        $booking = $serializer->deserialize($request->getContent(), Booking::class, 'json');
        $violations = $validator->validate($booking);
        if (\count($violations) > 0) {
            $errors = [];
            foreach ($violations as $violation) {
                $errors[] = ['message' => $violation->getMessage(), 'field' => $violation->getPropertyPath()];
            }

            return new JsonResponse($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($booking);
        $em->flush();

        return JsonResponse::fromJsonString($serializer->serialize($booking, 'json'), Response::HTTP_CREATED);
    }

    /**
     * @Route("/equipment/demands", methods={"GET"})
     * @param BookingEquipmentService $bookingEquipmentService
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function getBookingEquipmentDemands(
        BookingEquipmentService $bookingEquipmentService,
        SerializerInterface $serializer
    ): Response {
        $demands = $bookingEquipmentService->getEquipmentDemands();
        return JsonResponse::fromJsonString($serializer->serialize($demands, 'json'));
    }
}

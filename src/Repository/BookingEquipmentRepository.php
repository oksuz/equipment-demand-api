<?php

namespace App\Repository;

use App\Entity\BookingEquipment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class BookingEquipmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BookingEquipment::class);
    }

    public function findEquipmentUsages()
    {

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select([
                'b.id AS bookingId',
                'b.bookDate',
                'b.returnDate',
                'l1.id AS bookFromId',
                'l1.name AS bookFrom',
                'l2.id AS returnToId',
                'l2.name AS returnTo',
                'l3.id AS equipmentLocationId',
                'l3.name AS equipmentLocation',
                'be.quantity AS desiredQuantity',
                'eq2.quantity AS inventorySize',
                'eq.name AS equipmentName',
                'eq.id AS equipmentId'
            ])
                ->addSelect('(
                    SELECT SUM(be2.quantity) FROM App\Entity\BookingEquipment be2
                    JOIN App\Entity\Booking b2 WITH b2.id = be2.booking
                    WHERE b2.bookDate <= b.bookDate 
                    AND b2.returnDate > b.bookDate AND b.id != b2.id
                    AND be2.equipment = eq
                ) AS equipmentInUse')
                ->addSelect('(
                    SELECT GROUP_CONCAT(b3.id) FROM App\Entity\BookingEquipment be3
                    JOIN App\Entity\Booking b3 WITH b3 = be3.booking
                    WHERE b3.returnDate <= b.bookDate 
                        AND b3.returnTo = b.bookFrom 
                        AND b3.id != b.id 
                        AND be3.equipment = eq
                ) AS equipmentWillReturnFromRental')
            ->from('App\\Entity\\BookingEquipment', 'be')
            ->innerJoin('be.booking', 'b')
            ->leftJoin('be.equipment', 'eq')
            ->leftJoin('be.equipment', 'eq2', 'WITH', 'eq2.location = b.bookFrom')
            ->leftJoin('b.bookFrom', 'l1')
            ->leftJoin('b.returnTo', 'l2')
            ->leftJoin('eq.location', 'l3')
            ->orderBy('b.bookDate, b.id');


        return $queryBuilder->getQuery()->getResult();

        /**
         * following sql is equal to above dql


        return $this->getEntityManager()->getConnection()->executeQuery('
          SELECT
            b.id AS bookingId,
            b.book_date AS bookDate,
            b.return_date AS returnDate,
            l1.name AS bookFrom,
            l2.name AS returnTo,
            l1.id AS bookFromId,
            l2.id AS returnToId,
            eq.location AS equipmentLocationId,
            l3.name AS equipmentLocation,
            be.quantity AS desiredQuantity,
            eq2.quantity AS inventorySize,
            eq.name AS equipmentName,

            (
              SELECT SUM(be2.quantity) FROM booking_equipment be2
              INNER JOIN booking b2 ON b2.id = be2.booking_id
              WHERE b2.book_date <= b.book_date
                AND b2.return_date > b.book_date
                AND be2.equipment_id = eq2.id AND b2.id != b.id ORDER BY b2.book_date, b2.id ASC
            ) AS equipmentInUse,

            (
              SELECT SUM(be2.quantity) FROM booking_equipment be2
              INNER JOIN booking b2 ON b2.id = be2.booking_id
              WHERE b2.return_date <= b.book_date AND b2.return_to = b.book_from
                AND b2.id != b.id AND be2.equipment_id = eq2.id
            ) AS equipmentWillReturn

          FROM booking_equipment be

          INNER JOIN booking b ON b.id = be.booking_id
          LEFT JOIN equipment eq ON eq.id = be.equipment_id
          LEFT JOIN equipment eq2 ON eq2.id = be.equipment_id AND eq2.location = b.book_from
          LEFT JOIN location l1 ON b.book_from = l1.id
          LEFT JOIN location l2 ON b.return_to = l2.id
          LEFT JOIN location l3 ON eq.location = l3.id
          ORDER BY b.book_date, b.id ASC
        ')->fetchAllAssociative();
         **/
    }
}
